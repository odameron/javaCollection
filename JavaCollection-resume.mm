<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Java Collection Framework" FOLDED="false" ID="ID_880336193" CREATED="1692308343285" MODIFIED="1692308370501" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="5" RULE="ON_BRANCH_CREATION"/>
<node TEXT="principe" POSITION="right" ID="ID_770857484" CREATED="1692603814597" MODIFIED="1692603818356">
<edge COLOR="#ff00ff"/>
<node TEXT="la librairie est assez riche" ID="ID_601623084" CREATED="1692603820013" MODIFIED="1692603836444"/>
<node TEXT="la difficult&#xe9; consiste &#xe0; faire ressortir une organisation" ID="ID_606840966" CREATED="1692603842910" MODIFIED="1692603883669"/>
<node TEXT="sch&#xe9;ma g&#xe9;n&#xe9;ral (simplifi&#xe9;)" ID="ID_65339918" CREATED="1692603893453" MODIFIED="1692955241189">
<node TEXT="https://gitlab.com/odameron/javaCollection/-/blob/main/javaCollectionHierarchy.png" ID="ID_1991674087" CREATED="1692603964262" MODIFIED="1692603971902">
<icon BUILTIN="internet"/>
</node>
</node>
<node TEXT="adapt&#xe9;e au cas g&#xe9;n&#xe9;ral" ID="ID_407443100" CREATED="1692604019101" MODIFIED="1692604081716">
<icon BUILTIN="idea"/>
<node TEXT="il faut savoir que &#xe7;a existe" ID="ID_226847965" CREATED="1692604032302" MODIFIED="1692604039109"/>
<node TEXT="il faut savoir (bien) l&apos;utiliser" ID="ID_1875993915" CREATED="1692604039414" MODIFIED="1692604049286"/>
</node>
<node TEXT="pas forc&#xe9;ment adapt&#xe9;e &#xe0; votre cas particulier" ID="ID_1136922469" CREATED="1692604051750" MODIFIED="1692604071414">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node TEXT="le plus souvent" POSITION="right" ID="ID_1721709464" CREATED="1692955328710" MODIFIED="1692965313836">
<icon BUILTIN="help"/>
<edge COLOR="#00ffff"/>
<node TEXT="besoin de parcourir les &#xe9;l&#xe9;ments" ID="ID_1634913507" CREATED="1692308376344" MODIFIED="1692955334806">
<icon BUILTIN="help"/>
<node TEXT="interface Iterable" ID="ID_409914313" CREATED="1692308468688" MODIFIED="1692313766749">
<icon BUILTIN="executable"/>
<node TEXT="m&#xe9;thodes principales" ID="ID_1384862118" CREATED="1692374266077" MODIFIED="1692374280512" COLOR="#ff0000">
<font BOLD="true"/>
<node TEXT="Iterator&lt;T&gt; iterator()" ID="ID_142616184" CREATED="1692912874101" MODIFIED="1692912982859">
<font NAME="Courier" BOLD="true"/>
</node>
<node TEXT="void forEach()" ID="ID_1478082358" CREATED="1692912868624" MODIFIED="1692913001242">
<font NAME="Courier"/>
</node>
</node>
<node TEXT="g&#xe9;n&#xe9;ricit&#xe9; et &quot;diamond operator&quot;" ID="ID_569913425" CREATED="1692794954046" MODIFIED="1692794968580">
<node TEXT="TODO" ID="ID_124427919" CREATED="1692374282558" MODIFIED="1692374286954">
<icon BUILTIN="pencil"/>
</node>
</node>
<node TEXT="cr&#xe9;ation" ID="ID_410680401" CREATED="1692603474021" MODIFIED="1692603477324">
<node TEXT="Iterable&lt;T&gt; someIterable = new ClassImplementingIterable(...)" ID="ID_518686426" CREATED="1692913054173" MODIFIED="1692913114567">
<font NAME="Courier"/>
</node>
<node TEXT="declaration type = interface" ID="ID_797328136" CREATED="1692913127853" MODIFIED="1692913185661"/>
<node TEXT="initialization = class implementing the interface" ID="ID_1047936328" CREATED="1692913136797" MODIFIED="1692913167059"/>
</node>
<node TEXT="parcours" ID="ID_1421213829" CREATED="1692603479028" MODIFIED="1692603481475">
<node TEXT="parcours avec enhanced for" ID="ID_167650914" CREATED="1692312571977" MODIFIED="1692314591428">
<icon BUILTIN="bookmark"/>
<font BOLD="true"/>
<node TEXT="simple when" ID="ID_115057406" CREATED="1692314540557" MODIFIED="1692314961066">
<node TEXT="you want to iterate over all the elements" ID="ID_1267616571" CREATED="1692314934404" MODIFIED="1692314944707"/>
<node TEXT="you do not need to modify the collection" ID="ID_994901031" CREATED="1692314945119" MODIFIED="1692314955595"/>
</node>
<node TEXT="syntactic sugar over iterator" ID="ID_292967776" CREATED="1692314509549" MODIFIED="1692314520331"/>
<node TEXT="for(EltType elt : monIterable) {&#xa;  /* something with elt */&#xa;}" ID="ID_718770086" CREATED="1692312580824" MODIFIED="1692312693689">
<font NAME="Courier"/>
</node>
</node>
<node TEXT="parcours avec it&#xe9;rateur" ID="ID_1918932589" CREATED="1692308496143" MODIFIED="1692314594011">
<icon BUILTIN="bookmark"/>
<font BOLD="true"/>
<node TEXT="when" ID="ID_1250057572" CREATED="1692315009462" MODIFIED="1692315015019">
<node TEXT="you may abort the iteration if a condition becomes true" ID="ID_1931369064" CREATED="1692315028974" MODIFIED="1692315063386"/>
<node TEXT="you need to modify the collection" ID="ID_1416405290" CREATED="1692315016910" MODIFIED="1692315028619"/>
</node>
<node TEXT="Iterator&lt;T&gt; iter = monIterable.iterator();&#xa;while (iter.hasNext()) {&#xa;  EltType elt = iter.next();&#xa;  /* something with elt */&#xa;}" ID="ID_1527642954" CREATED="1692313958165" MODIFIED="1692314146106">
<font NAME="Courier"/>
</node>
</node>
</node>
<node TEXT="interface Collection" ID="ID_1698731310" CREATED="1692308530368" MODIFIED="1692366667907" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
<node TEXT="m&#xe9;thodes" ID="ID_1905513965" CREATED="1692308658152" MODIFIED="1692308677878">
<node TEXT="2 constructeurs" ID="ID_1781373292" CREATED="1692308679087" MODIFIED="1692308683795">
<node TEXT="vide" ID="ID_610659474" CREATED="1692308689640" MODIFIED="1692308692204"/>
<node TEXT="1 param&#xe8;tre Collection" ID="ID_640715428" CREATED="1692308692512" MODIFIED="1692308737068"/>
</node>
<node TEXT="int size()" ID="ID_841299919" CREATED="1692308754224" MODIFIED="1692374531901">
<font NAME="Courier"/>
</node>
<node TEXT="boolean isEmpty()" ID="ID_317930771" CREATED="1692308750192" MODIFIED="1692374540053">
<font NAME="Courier"/>
</node>
<node TEXT="boolean add(E e)" ID="ID_1293489242" CREATED="1692308756808" MODIFIED="1692374558988">
<font NAME="Courier"/>
<node TEXT="returns true if e was not already present" ID="ID_1548943964" CREATED="1692374567719" MODIFIED="1692374579372"/>
</node>
<node TEXT="void clear()" ID="ID_1531441460" CREATED="1692308770792" MODIFIED="1692374591788">
<font NAME="Courier"/>
</node>
<node TEXT="boolean contains(E e)" ID="ID_978419588" CREATED="1692308774608" MODIFIED="1692374598045">
<font NAME="Courier"/>
</node>
<node TEXT="boolean remove(O o)" ID="ID_1582419911" CREATED="1692308795944" MODIFIED="1692374608645">
<font NAME="Courier"/>
</node>
</node>
<node TEXT="pas de doublon" ID="ID_871941279" CREATED="1692308591015" MODIFIED="1692308602571">
<icon BUILTIN="help"/>
<node TEXT="interface Set" ID="ID_1044694918" CREATED="1692308612304" MODIFIED="1692366647231" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
<node TEXT="m&#xe9;thodes principales" ID="ID_1621412408" CREATED="1692374155204" MODIFIED="1692374179629" COLOR="#ff0000">
<font BOLD="true"/>
<node TEXT="m&#xe9;thodes Collection" ID="ID_1494855575" CREATED="1692374165085" MODIFIED="1692374170747"/>
<node TEXT="boolean add(E e)" ID="ID_394731109" CREATED="1692374350182" MODIFIED="1692374518481">
<font NAME="Courier"/>
</node>
<node TEXT="boolean contains(E e)" ID="ID_932693012" CREATED="1692374359885" MODIFIED="1692374510560">
<font NAME="Courier"/>
</node>
<node TEXT="boolean remove(Object o)" ID="ID_721364428" CREATED="1692374369542" MODIFIED="1692374510563">
<font NAME="Courier"/>
</node>
</node>
<node TEXT="ordre parcours pas important" ID="ID_554291800" CREATED="1692308865048" MODIFIED="1692308877684">
<icon BUILTIN="help"/>
<node TEXT="classe HashSet" ID="ID_1413731425" CREATED="1692309894795" MODIFIED="1692366573782" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
</node>
</node>
<node TEXT="ordre parcours = ordre d&apos;ajout" ID="ID_1913934963" CREATED="1692308924432" MODIFIED="1692308941417">
<icon BUILTIN="help"/>
<node TEXT="pas forc&#xe9;ment d&apos;ordre entre les &#xe9;l&#xe9;ments" ID="ID_576967886" CREATED="1692308974712" MODIFIED="1692309000420"/>
<node TEXT="(ou on n&apos;a pas besoin d&apos;utiliser l&apos;ordre entre les elts)" ID="ID_1683071663" CREATED="1692309001144" MODIFIED="1692309014636"/>
<node TEXT="classe LinkedHashSet" ID="ID_1666975184" CREATED="1692309029776" MODIFIED="1692309936147">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
</node>
</node>
<node TEXT="ordre parcours = ordre entre les &#xe9;l&#xe9;ments" ID="ID_1828427028" CREATED="1692308943936" MODIFIED="1692308967530">
<icon BUILTIN="help"/>
<node TEXT="on peut l&apos;exploiter" ID="ID_1283088554" CREATED="1692309168041" MODIFIED="1692309184779"/>
<node TEXT="mais maintenir la collection tri&#xe9;e peut co&#xfb;ter cher" ID="ID_1522582769" CREATED="1692309185232" MODIFIED="1692309210396"/>
<node TEXT="interface SortedSet" ID="ID_665171788" CREATED="1692309211153" MODIFIED="1692309235294">
<icon BUILTIN="executable"/>
<node TEXT="classe TreeSet" ID="ID_1452498254" CREATED="1692309218440" MODIFIED="1692369915573" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="doublons possibles" ID="ID_1054442952" CREATED="1692309495257" MODIFIED="1692309503642">
<icon BUILTIN="help"/>
<node TEXT="acc&#xe8;s = ajouter, supprimer, consulter" ID="ID_345133036" CREATED="1692309562513" MODIFIED="1692309577050"/>
<node TEXT="acc&#xe8;s en d&#xe9;but ou en fin" ID="ID_162338832" CREATED="1692309508697" MODIFIED="1692363072349">
<icon BUILTIN="help"/>
<node TEXT="interface Queue" ID="ID_1538429810" CREATED="1692309587593" MODIFIED="1692366647234" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
<node TEXT="m&#xe9;thodes principales" ID="ID_1286855044" CREATED="1692373687467" MODIFIED="1692373930378" COLOR="#ff0000">
<font BOLD="true"/>
<node TEXT="m&#xe9;thodes Collection" ID="ID_707712559" CREATED="1692362744672" MODIFIED="1692373700462"/>
<node TEXT="boolean add(E e) / boolean offer(E e)" ID="ID_657807608" CREATED="1692373717596" MODIFIED="1692373887444">
<font NAME="Courier"/>
<node TEXT="insert (at the head or tail)" ID="ID_1831636404" CREATED="1692374031236" MODIFIED="1692374038186"/>
</node>
<node TEXT="E remove() / E poll()" ID="ID_1743244828" CREATED="1692373839972" MODIFIED="1692373887445">
<font NAME="Courier"/>
<node TEXT="remove the head" ID="ID_1706150168" CREATED="1692374057005" MODIFIED="1692374063491"/>
</node>
<node TEXT="E element() / E peek()" ID="ID_765319802" CREATED="1692373864771" MODIFIED="1692373887445">
<font NAME="Courier"/>
<node TEXT="return the head" ID="ID_738484984" CREATED="1692374083981" MODIFIED="1692374087690"/>
</node>
</node>
<node TEXT="types classiques" ID="ID_1656703868" CREATED="1692601675445" MODIFIED="1692601715189">
<icon BUILTIN="bookmark"/>
<node TEXT="FIFO = file d&apos;attente" ID="ID_286989124" CREATED="1692601643161" MODIFIED="1692601724512">
<font BOLD="true"/>
</node>
<node TEXT="LIFO = pile" ID="ID_157901905" CREATED="1692601694175" MODIFIED="1692601726747">
<font BOLD="true"/>
</node>
<node TEXT="heap = tas" ID="ID_958015898" CREATED="1692602826874" MODIFIED="1692602834602">
<font BOLD="true"/>
<node TEXT="https://en.wikipedia.org/wiki/Heap_(data_structure)" ID="ID_197262374" CREATED="1692602847634" MODIFIED="1692602849211"/>
<node TEXT="quand vous avez besoin de trouver le plus grand (ou le plus petit) &#xe9;l&#xe9;ment d&apos;une collection sans trier tous les autres &#xe9;l&#xe9;ments" ID="ID_734159219" CREATED="1692965389794" MODIFIED="1692965511230">
<icon BUILTIN="idea"/>
</node>
</node>
</node>
<node TEXT="ordre par d&#xe9;faut = FIFO" ID="ID_1385221387" CREATED="1692309772610" MODIFIED="1692311350488">
<icon BUILTIN="help"/>
<node TEXT="add(e)" ID="ID_844656639" CREATED="1692310566860" MODIFIED="1692310711157">
<font NAME="Courier"/>
<node TEXT="insert at the tail" ID="ID_1163570084" CREATED="1692310633596" MODIFIED="1692310679848"/>
</node>
<node TEXT="remove()" ID="ID_1274207136" CREATED="1692310654092" MODIFIED="1692310711157">
<font NAME="Courier"/>
<node TEXT="remove the head" ID="ID_648951737" CREATED="1692310660636" MODIFIED="1692310667568"/>
</node>
<node TEXT="element()" ID="ID_967448764" CREATED="1692310688621" MODIFIED="1692310711154">
<font NAME="Courier"/>
<node TEXT="return the head" ID="ID_1464846426" CREATED="1692310692485" MODIFIED="1692310698968"/>
</node>
<node TEXT="capacit&#xe9; max" ID="ID_1338769722" CREATED="1692313674898" MODIFIED="1692313685054">
<icon BUILTIN="help"/>
<node TEXT="classe ArrayBlockingQueue" ID="ID_552620529" CREATED="1692313685059" MODIFIED="1692366560542" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
</node>
</node>
<node TEXT="capacit&#xe9; illimit&#xe9;e" ID="ID_663516980" CREATED="1692313702923" MODIFIED="1692313713976">
<icon BUILTIN="help"/>
<node TEXT="classe PriorityQueue" ID="ID_1530520765" CREATED="1692313725228" MODIFIED="1692373512304" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<font SIZE="10" BOLD="false"/>
</node>
<node TEXT="classe LinkedList" ID="ID_633035625" CREATED="1692313871388" MODIFIED="1692313883488">
<icon BUILTIN="executable"/>
</node>
</node>
</node>
<node TEXT="ajout en d&#xe9;but ou en fin" ID="ID_107569118" CREATED="1692311353445" MODIFIED="1692311361570">
<icon BUILTIN="help"/>
<node TEXT="interface Deque" ID="ID_1862720760" CREATED="1692310926877" MODIFIED="1692372697563" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<font SIZE="12" BOLD="true"/>
<node TEXT="double-ended Queue" ID="ID_188964668" CREATED="1692310944301" MODIFIED="1692310952097"/>
<node TEXT="methodes" ID="ID_262156143" CREATED="1692310967686" MODIFIED="1692374106427" COLOR="#ff0000">
<font BOLD="true"/>
<node TEXT="addFirst(e)" ID="ID_1164757590" CREATED="1692310987957" MODIFIED="1692311041290">
<font NAME="Courier"/>
</node>
<node TEXT="addLast(e)" ID="ID_1628210849" CREATED="1692310991877" MODIFIED="1692311041295">
<font NAME="Courier"/>
</node>
<node TEXT="removeFirst()" ID="ID_1948156725" CREATED="1692310996092" MODIFIED="1692311041296">
<font NAME="Courier"/>
</node>
<node TEXT="removeLast()" ID="ID_1825835022" CREATED="1692311003933" MODIFIED="1692311041296">
<font NAME="Courier"/>
</node>
<node TEXT="getFirst()" ID="ID_234108918" CREATED="1692311021405" MODIFIED="1692311041297">
<font NAME="Courier"/>
</node>
<node TEXT="getLast()" ID="ID_181986853" CREATED="1692311026221" MODIFIED="1692311041297">
<font NAME="Courier"/>
</node>
</node>
<node TEXT="peut &#xea;tre FIFO" ID="ID_1826742871" CREATED="1692373562315" MODIFIED="1692373569112"/>
<node TEXT="peut &#xea;tre LIFO" ID="ID_1949820325" CREATED="1692309778546" MODIFIED="1692309783598">
<node TEXT="push(e) = addFirst(e)" ID="ID_625470754" CREATED="1692310792213" MODIFIED="1692310870788">
<font NAME="Courier"/>
</node>
<node TEXT="pop() = removeFirst()" ID="ID_1426573805" CREATED="1692310815741" MODIFIED="1692310870788">
<font NAME="Courier"/>
</node>
<node TEXT="peek() = peekFirst()" ID="ID_837814994" CREATED="1692310828437" MODIFIED="1692310870785">
<font NAME="Courier"/>
</node>
</node>
<node TEXT="classe ArrayDeque" ID="ID_1503157527" CREATED="1692311154661" MODIFIED="1692366549262" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
<node TEXT="faster than Stack when used as a stack" ID="ID_396369965" CREATED="1692373409161" MODIFIED="1692373577338">
<icon BUILTIN="idea"/>
<font BOLD="true"/>
</node>
<node TEXT="faster than LinkedList when used as a queue" ID="ID_1453089958" CREATED="1692373421361" MODIFIED="1692373579502">
<icon BUILTIN="idea"/>
<font BOLD="true"/>
</node>
</node>
<node TEXT="classe LinkedList" ID="ID_82690272" CREATED="1692311162453" MODIFIED="1692311171785">
<icon BUILTIN="executable"/>
</node>
</node>
</node>
<node TEXT="exploiter ordre sur les elts" ID="ID_896188778" CREATED="1692309784378" MODIFIED="1692311373963">
<icon BUILTIN="help"/>
<node TEXT="classe PriorityQueue" ID="ID_1065060718" CREATED="1692309845210" MODIFIED="1692366533732" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
<node TEXT="heap" ID="ID_1544777756" CREATED="1692602997042" MODIFIED="1692602999990"/>
</node>
</node>
</node>
</node>
<node TEXT="acc&#xe8;s n&apos;importe o&#xf9;" ID="ID_365179462" CREATED="1692309522961" MODIFIED="1692309533581">
<icon BUILTIN="help"/>
<node TEXT="interface List" ID="ID_526166767" CREATED="1692309602193" MODIFIED="1692366513349" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
<node TEXT="ordered collection (also known as a sequence)" ID="ID_448966724" CREATED="1692362629976" MODIFIED="1692362631260"/>
<node TEXT="plus complexe que Queue donc co&#xfb;te plus cher" ID="ID_844108829" CREATED="1692312195216" MODIFIED="1692312205387"/>
<node TEXT="m&#xe9;thodes" ID="ID_512747004" CREATED="1692372923344" MODIFIED="1692372926848">
<node TEXT="idem Collection + acc&#xe8;s par position" ID="ID_159669643" CREATED="1692362815600" MODIFIED="1692372943038">
<node TEXT="iterating over the elements in a list is typically preferable to indexing through it if the caller does not know the implementation" ID="ID_1660746391" CREATED="1692362958857" MODIFIED="1692362960221"/>
<node TEXT="special iterator, called a ListIterator, that allows element insertion and replacement, and bidirectional access" ID="ID_486502939" CREATED="1692362991025" MODIFIED="1692362996270"/>
</node>
</node>
<node TEXT="reading more frequent than adding or removing" ID="ID_1895082175" CREATED="1692366064436" MODIFIED="1692366081991">
<icon BUILTIN="help"/>
<node TEXT="classe ArrayList" ID="ID_68100989" CREATED="1692363038521" MODIFIED="1692366513345" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
<node TEXT="tableau dynamique" ID="ID_377729354" CREATED="1692979188298" MODIFIED="1692979194739"/>
<node TEXT="Resizable-array implementation of the List interface" ID="ID_746726563" CREATED="1692363150977" MODIFIED="1692363151898"/>
</node>
</node>
<node TEXT="adding or removing more frequent than reading" ID="ID_1378015079" CREATED="1692366090652" MODIFIED="1692366118537">
<icon BUILTIN="help"/>
<node TEXT="classe LinkedList" ID="ID_1804761311" CREATED="1692363480045" MODIFIED="1692366520255" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
<node TEXT="liste doublement cha&#xee;n&#xe9;e" ID="ID_1275580633" CREATED="1692373091713" MODIFIED="1692373098855"/>
<node TEXT="Doubly-linked list implementation of the List and Deque interfaces" ID="ID_120554898" CREATED="1692365855602" MODIFIED="1692365857449"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="besoin d&apos;acc&#xe9;der &#xe0; un &#xe9;lement" ID="ID_599001720" CREATED="1692308398951" MODIFIED="1692955336774">
<icon BUILTIN="help"/>
<node TEXT="interface Map" ID="ID_1598205171" CREATED="1692308541487" MODIFIED="1692366667903" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
<node TEXT="maps a group of keys to their respective value" ID="ID_1893592536" CREATED="1692366764823" MODIFIED="1692979297572"/>
<node TEXT="cannot contain duplicate keys" ID="ID_1545625341" CREATED="1692366766519" MODIFIED="1692979512448">
<node TEXT="each key can map to at most one value" ID="ID_1529843608" CREATED="1692979513652" MODIFIED="1692979516513"/>
<node TEXT="if necessary, the value acan be a collection" ID="ID_1421586069" CREATED="1692979521916" MODIFIED="1692979693159">
<node TEXT="num &#xe9;tudiant -&gt; 1 &#xe9;tudiant.e" ID="ID_885615353" CREATED="1692979632868" MODIFIED="1692979672695"/>
<node TEXT="ident promo -&gt; {n &#xe9;tudiant.e.s}" ID="ID_861818980" CREATED="1692979644764" MODIFIED="1692979667869"/>
</node>
</node>
<node TEXT="m&#xe9;thodes principales" ID="ID_1001919984" CREATED="1692367001288" MODIFIED="1692374677661" COLOR="#ff0000">
<font BOLD="true"/>
<node TEXT="2 constructeurs" ID="ID_139706194" CREATED="1692367008776" MODIFIED="1692367012749">
<node TEXT="vide" ID="ID_1776317647" CREATED="1692367020680" MODIFIED="1692367022508"/>
<node TEXT="Map" ID="ID_247071282" CREATED="1692367022792" MODIFIED="1692367024260"/>
</node>
<node TEXT="V put(K key, V value)" ID="ID_864573815" CREATED="1692374719207" MODIFIED="1692375044492">
<font NAME="Courier"/>
<node TEXT="associe la valeur &#xe0; la cl&#xe9;" ID="ID_153517122" CREATED="1692374757287" MODIFIED="1692374765334"/>
<node TEXT="&#xe9;crase l&apos;ancienne valeur si la cl&#xe9; &#xe9;tait d&#xe9;j&#xe0; pr&#xe9;sente" ID="ID_182599909" CREATED="1692374771631" MODIFIED="1692374793397"/>
</node>
<node TEXT="boolean containsKey(Object key)" ID="ID_1070899666" CREATED="1692375101465" MODIFIED="1692375133758">
<font NAME="Courier"/>
</node>
<node TEXT="V get(Object key)" ID="ID_621189993" CREATED="1692374810112" MODIFIED="1692375044492">
<font NAME="Courier"/>
<node TEXT="renvoie la valeur associ&#xe9;e &#xe0; la cl&#xe9;, ou null" ID="ID_629403174" CREATED="1692374833416" MODIFIED="1692374843653"/>
</node>
</node>
<node TEXT="structure similaire &#xe0; Set (map = ensemble de paires (cl&#xe9;, valeur))" ID="ID_1193440073" CREATED="1692979871595" MODIFIED="1692979902146"/>
<node TEXT="pas d&apos;ordre sur les cl&#xe9;s" ID="ID_440661009" CREATED="1692369056800" MODIFIED="1692369065319">
<icon BUILTIN="help"/>
<node TEXT="classe HashMap" ID="ID_1386935955" CREATED="1692367074576" MODIFIED="1692369289362" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
</node>
<node TEXT="plus rapide que Hashtable" ID="ID_1704305379" CREATED="1692369402882" MODIFIED="1692369600662">
<icon BUILTIN="idea"/>
</node>
<node TEXT="plus rapide que TreeMap, mais n&#xe9;cessite plus de m&#xe9;moire" ID="ID_1845710801" CREATED="1692369527346" MODIFIED="1692369736568">
<icon BUILTIN="idea"/>
</node>
</node>
<node TEXT="ordre sur l&apos;insertion des cl&#xe9;s" ID="ID_1652369107" CREATED="1692370006036" MODIFIED="1692370017533">
<icon BUILTIN="help"/>
<node TEXT="classe LinkedHashMap" ID="ID_1187500184" CREATED="1692370047162" MODIFIED="1692370074487" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
</node>
</node>
<node TEXT="ordre sur les valeurs des cl&#xe9;s" ID="ID_1128206233" CREATED="1692369046032" MODIFIED="1692370002505">
<icon BUILTIN="help"/>
<node TEXT="order is reflected when iterating over the sorted map&apos;s collection views" ID="ID_1735859875" CREATED="1692369137857" MODIFIED="1692369170961">
<node TEXT="entrySet(...)" ID="ID_925563252" CREATED="1692369144455" MODIFIED="1692369185921">
<font NAME="Courier"/>
</node>
<node TEXT="keySet(...)" ID="ID_698168667" CREATED="1692369153279" MODIFIED="1692369185928">
<font NAME="Courier"/>
</node>
<node TEXT="values(...)" ID="ID_59936576" CREATED="1692369159449" MODIFIED="1692369185929">
<font NAME="Courier"/>
</node>
</node>
<node TEXT="interface SortedMap" ID="ID_304645485" CREATED="1692367110095" MODIFIED="1692369289366" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
<node TEXT="classe TreeMap" ID="ID_1264241067" CREATED="1692367081776" MODIFIED="1692369289365" COLOR="#0033ff">
<icon BUILTIN="executable"/>
<icon BUILTIN="bookmark"/>
<font SIZE="12" BOLD="true"/>
<node TEXT="n&#xe9;cessite moins de m&#xe9;moire que HashMap mais plus lent" ID="ID_902013705" CREATED="1692369706356" MODIFIED="1692369722653">
<icon BUILTIN="idea"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</map>
